
$(function(){
     
    //"Surveillance du champ "medicament" avec la méthode jQuery "keyup"
    
   
    $("#medicament").keyup(function(){
          
        //on efface le champ résultat
        $("#resultat").html('');
        //Si le champ "medicament" contient plus d'un caractère, l'appel Ajax est déclenché
        if ($("#medicament").val().length>=2){
            $.ajax({
                type:'POST',
                
                url:"RequeteAjax/general ",
                //on envoi ici des informations au script php "rechercheajaxphp.php",
                //le script php va recevoir l'info (data) sous la forme $_POST['medicament']
                //comme si la variable avait été envoyée par le formulaire directement
                //(utile en l'absence de JavaScript)
                data: 'recherche='+$("#medicament").val(),
                //Avant d'envoyer les infos, on fait apparaître le loader
                //beforeSend:function(){$("img[alt='loader']").css("display","inline");},
                //Le script php nous a renvoyé du HTML (paramètre "code_html"
                //contenant le fruit de la recherche dans la base de données)
                success:function(code_html){
                    //on fait disparaître le loader
                    //$("img[alt='loader']").css("display","none");
                    //on fait apparaître le résultat dans la div d'id "resultat"
                    $("#infos_france").html(code_html);
                }
            });        
        }
        else {
            $("#resultat").html('');
        }
    });


});
