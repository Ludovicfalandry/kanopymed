<?php


class RequeteAjax extends CI_Controller {

    public function general(){
        echo ("dferg&rggrs");
        if ($this->input->is_ajax_request()) {
            $bd = $this->bd;
            //Vérification de l'existence de $_POST['medicament'] (cette variable peut avoir 2 provenances : en temps "normal" elle provient du fichier javaScript (requête Ajax)
// en cas de désactivation du JavavaScript la variable provient du formulaire (fichier rechercheajax.html)).
/* var_dump($_POST); */
if (!empty($_POST['recherche'])){
    //la suite est classique (voir la partie "Lire les commentaires")
    $search=htmlspecialchars($_POST['recherche']);
   
    $general= $this->bd->prepare("SELECT SUM(BOITES)as general_boite, AVG(age) as general_age , SUM(REM) as rembAssocFr FROM PHEV_2017");
    $general->execute();
    $stat=$general->fetch();
   
    //var_dump($stat['general_age']);
    
    //Analysez bien la requête sql suivante notamment la partie "WHERE pseudo LIKE '$search%' OR commentaire LIKE '%$search%'", à quoi sert le signe % ?
    $reponse= $this->bd->prepare("SELECT SUM(BOITES)as nombre_boite , AVG(age) as age_moyen_dep , SUM(REM) as rembAssocDep FROM PHEV_2017 WHERE code_postal LIKE '%$search%' ");
    $reponse->execute();
    

    echo '<h3>Résultats de la recherche pour le département"'.$search.'"</h3>';
    //la ligne suivante vérifie qu'il y a des réponses
    if ($reponse->rowCount()>0){ //
        while($donnee=$reponse->fetch()){
            /* var_dump($donnee['age_moyen_dep']); */
            $pourcentage_boite = ($donnee['nombre_boite'] * 100)/ $stat['general_boite'];
            $pourcentage_boite = number_format ( $pourcentage_boite );
            $stat['general_age'] = number_format ( $stat['general_age'] );
            $pourcentage_age_dp = ($donnee['age_moyen_dep']*100)/99;
            $pourcRem = ($donnee['rembAssocDep']*100)/$stat['rembAssocFr'];
            $pourcRem = number_format ($pourcRem);
            /* var_dump($pourcentage_boite); */
            echo '
            <div class="card">
                <div class="card-header">Statisique descriptive</div>
                    <div class="card-body">                        
                        <h5 class="card-title">Statistiques</h5>
                            <p class="card-text">
                            <p>

                                <div class="progress" id="national">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span id="legendeGeneral">'.$stat['general_boite'].'( 100% ) boites délivraient dans toute la france</span></div>
                                </div><br>

                                    <div class="progress" id="pourcentageRecherche">
                                        <div class="progress-bar progress-bar-striped  progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: '.$pourcentage_boite.'%"><span id="span">'.$donnee['nombre_boite']. '(' .$pourcentage_boite. '%) boites délivraient dans le département ' .$search. '</span></div>
                                    </div><br>
                                        <div class="progress" id="age_moyen">
                                            <div class="progress-bar progress-bar-striped  bg-warning progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 56%"><span id="legendeAgeFr">Age moyen en France est de '.$stat['general_age']. ' ans </span></div>
                                        </div><br>
                                            <div class="progress" id="age_moyen_departement">
                                                <div class="progress-bar progress-bar-striped  bg-warning progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: '.$pourcentage_age_dp.'%"><span id="legendeAgeDep">Age moyen du département est de '.$donnee['age_moyen_dep']. ' ans</span></div>
                                            </div><br>
                                                <div class="progress" id="RembAssocFrance">
                                                    <div class="progress-bar progress-bar-striped  bg-danger progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span id="legendeRemFr">Remboursement associé somme total en France ' .$stat['rembAssocFr'].' €</span></div>
                                                </div><br>
                                                    <div class="progress" id="RembAssocDep">
                                                        <div class="progress-bar progress-bar-striped  bg-danger progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: '.$pourcRem.'%"><span id="legendeRemDep">Remboursement associé somme total dans le departement ' .$donnee['rembAssocDep']. ' € ('.$pourcRem. '%)</span></div>
                                                    </div>
                            </p>
                            <h6>Légende :</h6>
                                <div id="legendeBoite">
                                    <span id="legendeColor"></span>Nombre de boites 
            
                                </div>
                                    <div id="legendeAge">
                                        <span id="legendeColor2"></span>Age moyen 
            
                                    </div>
                                        <div id="legendeRem">
                                            <span id="legendeColor3"></span>Remboursement Associé 
            
                                        </div>
                            </p>
   
                </div>
            </div>';
 
        }
    }
    else{
        echo '<p>Désolé, aucune réponse trouvée</p>';
    }
}
else{
    //Utile uniquement en cas de désactivation du JavavaScript
    echo'<p>Désolé, aucune réponse trouvée</p>';

}
        }
    }


}    