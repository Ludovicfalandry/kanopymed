<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="fr">
<head>
<link href="https://fonts.googleapis.com/css?family=Baloo+Tammudu|M+PLUS+Rounded+1c|Roboto" rel="stylesheet">    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/style_carte.css">
    <title>Kanopy</title>
</head>
<body>

<div class="container-fullwidth">
<!--  Menu -->
<nav class="navbar navbar-expand-lg navbar-light ">
                <a class="navbar-brand" href="/">KANOPY</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Domaine d'activité <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#">L'équipe</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Les Valeurs</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link " href="#">Contact</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link " href="<?php echo  base_url('dataVisualisation' ) ?>">Data visualisation</a>
                            </li>
                        </ul>
                    </div>
            </nav>